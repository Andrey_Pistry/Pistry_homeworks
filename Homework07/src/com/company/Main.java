package com.company;

/*
На вход подается последовательность чисел, оканчивающихся на -1.
Необходимо вывести число, которе присутствует в последовательности минимальное количество раз.
Гарантируется:
Все числа в диапазоне от -100 до 100.
Числа встречаются не более 2 147 483 647-раз каждое.
Сложность алгоритма - O(n)
 */


 import java.util.Scanner;
 public class Main {

     public static void main(String[] args) {

       int[] temp = new int[201]; // 201 элемент т.к. от 0 до 199, а по условию должно быть в диапозоне -100 до 100
       Scanner scan = new Scanner(System.in);
       System.out.println("Enter value from -100 to 100:");

       int dig = 0;
       while (dig != -1) {
          dig = scan.nextInt(); // введенные числа это индексы массива от -100 до 100
           if (dig != -1) {
               if ((dig >= -100) & (dig <= 100)) {
                   // сдвигаем индексы массива для отрицательных чисел + 100 и сразу суммируем повторы
                   temp[dig + 100] = temp[dig + 100] + 1;
               } else {
                   System.out.println("Only from -100 to 100, enter -1 to stop:"); // если введенное число за пределами -100 до 100
               }
           }
           else {
               System.out.println(" Stop");
           }
       }

       int minRepeatDigits = 2147483647; // Максимальное число по условию
         int memberIndex = 0; // искомое число - индекс массива
         // Ищем минимальное значение в массиве - его индекс  и есть суть искомое число
         for (int i = 0; i < 201; i++)
            if ((temp[i] < minRepeatDigits ) & (temp[i] != 0)) {
                minRepeatDigits=temp[i];
                memberIndex = i;
            }
         System.out.print("Searched number= " + (memberIndex - 100) + " Repeat " + minRepeatDigits);
     }
}
